import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ILaunchInfo {
  details: string;
  flight_id: string;
  flight_number: number;
  launch_year: number;
  links: {
    presskit: string;
  };
  mission_name: string;
  rocket: {
    rocket_name: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class SpaceXService {
  private readonly BASE_PATH = 'https://api.spacexdata.com/v3';
  private numberLaunchesCache = 0;

  constructor(
    private readonly http: HttpClient,
  ) { }

  getLaunchInfo(flightNumber: number): Observable<ILaunchInfo> {
    return this.http.get<ILaunchInfo>(`${this.BASE_PATH}/launches/${flightNumber}?filter=flight_id,flight_number,mission_name,links`);
  }

  getLaunchesList$(
    pageIndex: number,
    pageSize: number,
    sortField: string = '',
    sortOrder: string = ''
  ): Observable<ILaunchInfo[]> {

    const fieldsToGet = 'flight_id,flight_number,launch_year,rocket,details';
    const howManyToGet = `limit=${pageSize}&offset=${pageSize * pageIndex}`;
    const sortOptions = (sortField && sortOrder) ? `&sort=${sortField}&order=${sortOrder}` : '';
    // @todo: figure out why details is not sortable :-/

    return this.http.get<ILaunchInfo[]>(`${this.BASE_PATH}/launches?${howManyToGet}${sortOptions}&filter=${fieldsToGet}`);
    // @todo: check error case
  }

  getNumberOfLaunches$(): Observable<number> {
    // @todo: figure out appropriate time to invalidate this cache
    if (this.numberLaunchesCache) {
      return of(this.numberLaunchesCache);
    }

    return this.http.get<ILaunchInfo[]>(`${this.BASE_PATH}/launches?filter=flight_id`)
      .pipe(
        map((launchInfo: ILaunchInfo[]) => {
          this.numberLaunchesCache = launchInfo.length;

          return this.numberLaunchesCache;
        }),
      );
    // @todo: check error case
  }
}
