import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { SpaceXService, ILaunchInfo } from '../../services/space-x.service';
import { Subject, Observable } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-launches-list',
  templateUrl: './launches-list.component.html',
  styleUrls: ['./launches-list.component.scss']
})
export class LaunchesListComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<boolean>();

  displayedColumns: string[] = ['flight_number', 'launch_year', 'rocket_name', 'details'];
  launchData$: Observable<ILaunchInfo[]>;
  numLaunches$: Observable<number>;
  pageIndex: number;
  pageSize: number;
  pageSizeOptions: number[] = [10, 25, 50];
  sortActive: string;
  sortDirection: string;

  constructor(
    private readonly routes: ActivatedRoute,
    private readonly router: Router,
    private readonly spaceXService: SpaceXService,
  ) { }

  ngOnInit() {
    this.routes.paramMap
      .subscribe((params: ParamMap) => {
        this.pageIndex = +params.get('pageIndex') || 0;
        this.pageSize = +params.get('pageSize') || this.pageSizeOptions[0];
        this.sortActive = params.get('sortActive') || this.displayedColumns[0];
        this.sortDirection = params.get('sortDirection') || 'asc';
        this.numLaunches$ = this.spaceXService.getNumberOfLaunches$();

        this.launchData$ = this.spaceXService.getLaunchesList$(
          this.pageIndex,
          this.pageSize,
          this.sortActive,
          this.sortDirection,
        );
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  changePage(pageEvent: PageEvent): void {
    this.router.navigate(['launches', {
      pageIndex: pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
      sortActive: this.sortActive,
      sortDirection: this.sortDirection,
    }]);
  }

  changeSort(sortEvent: Sort): void {
    console.log(sortEvent);
    this.router.navigate(['launches', {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      sortActive: sortEvent.active,
      sortDirection: sortEvent.direction,
    }]);
 }

  rowClicked(flightNumber: number): void {
    this.router.navigate(['launch', flightNumber]);
    // @todo: check error case
  }
}
