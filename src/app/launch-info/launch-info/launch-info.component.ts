import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { switchMap } from 'rxjs/operators';

import { SpaceXService, ILaunchInfo } from '../../services/space-x.service';

@Component({
  selector: 'app-launch-info',
  templateUrl: './launch-info.component.html',
  styleUrls: ['./launch-info.component.scss']
})
export class LaunchInfoComponent implements OnInit {
  launchInfo: ILaunchInfo;
  pressKitUrl: SafeUrl;

  constructor(
    private readonly routes: ActivatedRoute,
    private readonly location: Location,
    private readonly sanitizer: DomSanitizer,
    private readonly spaceXService: SpaceXService,
  ) { }

  ngOnInit(): void {
    this.routes.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          const flightNumber = +params.get('id');

          return this.spaceXService.getLaunchInfo(flightNumber);
        })
      )
      .subscribe((launchInfo: ILaunchInfo) => {
        this.launchInfo = launchInfo;

        if (launchInfo.links && launchInfo.links.presskit) {
          this.pressKitUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            launchInfo.links.presskit.replace(/https?:/, '')
          );
        }
      });
  }

  goBack(): void {
    // @todo: need to figure out if there is a "back" to go to and if not, redirect via the router
    this.location.back();
  }
}
