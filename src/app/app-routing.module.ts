import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LaunchesListComponent } from './launches-list/launches-list/launches-list.component';
import { LaunchInfoComponent } from './launch-info/launch-info/launch-info.component';

const routes: Routes = [
  { path: '', redirectTo: '/launches', pathMatch: 'full' },
  { path: 'launches', component: LaunchesListComponent },
  { path: 'launch/:id', component: LaunchInfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
